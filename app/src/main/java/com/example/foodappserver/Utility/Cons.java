package com.example.foodappserver.Utility;



import com.example.foodappserver.Model.AddonModel;
import com.example.foodappserver.Model.FoodModel;
import com.example.foodappserver.Model.MenuCategoryModel;
import com.example.foodappserver.Model.SizeModel;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;

public class Cons {
    public static final String POPULAR_CAT_REF = "MostPopular";
    public static final String CAT_REF = "Category";
    public static final String Best_Ref = "BestDeals";
    public static final int DEFAULT_COLUMN_COUNT = 0;
    public static final int Full_Width_Column = 1;
    public static final String userName = "1";
    public static MenuCategoryModel categorySelected;
//    public static PopularCategoryModel popularCategoryModel;
    public static FoodModel selectedFood;

    public static String formatPrice(double displayPrice) {
        if (displayPrice != 0) {

            DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
            decimalFormat.setRoundingMode(RoundingMode.UP);
            String finalPrice = new String(decimalFormat.format(displayPrice).toString());
            return finalPrice.replace(".", ",");
        } else
            return "0.00";
    }

    public static Double calculatePriceExtra(SizeModel userSelectedSize, List<AddonModel> userSelectedAddOn) {
        Double result = 0.0;
        if (userSelectedAddOn  == null && userSelectedSize == null)
            return 0.0;
        else if (userSelectedSize == null)
        {
            for (AddonModel addonModel : userSelectedAddOn)
                result += addonModel.getPrice();
            return result;
        }
        else if (userSelectedAddOn == null)
        {
            return userSelectedSize.getPrice() * 1.0;
        }
        else
        {

            for (AddonModel addonModel : userSelectedAddOn)
                result += addonModel.getPrice();
            return result;
        }
    }

    public static String CreateOrderNumber() {

        return  new StringBuilder()
                .append(System.currentTimeMillis())
                .append(Math.abs(new Random().nextInt()))
                .toString();
    }

    public static String getDateOfWeek(int i) {

        switch (i)
        {
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
            default:
                return "Unk";
        }
        
    }

    public static String  convertToText(int staus) {
        switch (staus)
        {
            case 1:
                return "Placed";
            case 2:
                return "Shipping";
            case 3:
                return "Shipped";
            case 4:
                return "Deliveried";
            case 5:
                return "Cancel";
            default:
                return "Unk";

        }
    }
}
