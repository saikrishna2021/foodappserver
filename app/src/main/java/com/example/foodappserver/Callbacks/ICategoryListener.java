package com.example.foodappserver.Callbacks;

import com.example.foodappserver.Model.MenuCategoryModel;

import java.util.List;

public interface ICategoryListener {
     void  onCategorySuccess(List<MenuCategoryModel> menuCategoryModels);
    void  onCategoryFailed(String message);
}
