package com.example.foodappserver.ui.CategoryFragment;

import android.provider.SyncStateContract;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.foodappserver.Callbacks.ICategoryListener;
import com.example.foodappserver.Model.MenuCategoryModel;
import com.example.foodappserver.Utility.Cons;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CategoryViewModel extends ViewModel implements ICategoryListener {

    private MutableLiveData<List<MenuCategoryModel>>  listMutableLiveData;
    private ICategoryListener categoryListener;
    private MutableLiveData<String> messageError =  new MutableLiveData<>();

    public CategoryViewModel() {
        categoryListener = this;
    }

    public MutableLiveData<List<MenuCategoryModel>> getListMutableLiveData() {

        if (listMutableLiveData == null)
        {
            listMutableLiveData = new MutableLiveData<>();
            messageError =  new MutableLiveData<>();
            loadCategory();
        }

        return listMutableLiveData;
    }

    private void loadCategory() {
        final List<MenuCategoryModel> tempList = new ArrayList<>();
        final DatabaseReference categoryref  = FirebaseDatabase.getInstance().getReference(Cons.CAT_REF);
        categoryref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren())
                {
                    MenuCategoryModel model = dataSnapshot1.getValue(MenuCategoryModel.class);
                    model.setMenu_id(dataSnapshot1.getKey());
                    tempList.add(model);
                }

                categoryListener.onCategorySuccess(tempList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                categoryListener.onCategoryFailed(databaseError.getMessage());
            }
        });
    }


    public MutableLiveData<String> getMessageError() {
        return messageError;
    }
    @Override
    public void onCategorySuccess(List<MenuCategoryModel> menuCategoryModels) {
        listMutableLiveData.setValue(menuCategoryModels);
    }

    @Override
    public void onCategoryFailed(String message) {
        messageError.setValue(message);
    }
}