package com.example.foodappserver.Model;

import java.util.List;

public class FoodModel {
    private String id,name,image,description;
    private List<AddonModel> addon;
    private Long price;
    private List<SizeModel> size;

    private   List<AddonModel> userSelectedAddOn;
    private SizeModel userSelectedSize;

    public FoodModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AddonModel> getAddon() {
        return addon;
    }

    public void setAddon(List<AddonModel> addon) {
        this.addon = addon;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public List<SizeModel> getSize() {
        return size;
    }

    public void setSize(List<SizeModel> size) {
        this.size = size;
    }

    public List<AddonModel> getUserSelectedAddOn() {
        return userSelectedAddOn;
    }

    public void setUserSelectedAddOn(List<AddonModel> userSelectedAddOn) {
        this.userSelectedAddOn = userSelectedAddOn;
    }

    public SizeModel getUserSelectedSize() {
        return userSelectedSize;
    }

    public void setUserSelectedSize(SizeModel userSelectedSize) {
        this.userSelectedSize = userSelectedSize;
    }
}
