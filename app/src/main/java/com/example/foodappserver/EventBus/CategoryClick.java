package com.example.foodappserver.EventBus;


import com.example.foodappserver.Model.MenuCategoryModel;

public class CategoryClick {
    private boolean success;
    private MenuCategoryModel menuCategoryModel;

    public CategoryClick(boolean success, MenuCategoryModel menuCategoryModel) {
        this.success = success;
        this.menuCategoryModel = menuCategoryModel;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public MenuCategoryModel getMenuCategoryModel() {
        return menuCategoryModel;
    }

    public void setMenuCategoryModel(MenuCategoryModel menuCategoryModel) {
        this.menuCategoryModel = menuCategoryModel;
    }
}
