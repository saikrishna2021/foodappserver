package com.example.foodappserver.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.foodappserver.Callbacks.IRecyclerClickListener;
import com.example.foodappserver.EventBus.CategoryClick;
import com.example.foodappserver.Model.MenuCategoryModel;
import com.example.foodappserver.R;
import com.example.foodappserver.Utility.Cons;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.internal.Constants;


public class MyCategoryAdapter extends RecyclerView.Adapter<MyCategoryAdapter.MyViewHolder> {

    Context context;
    List<MenuCategoryModel> menuCategoryModelList;

    public MyCategoryAdapter(Context context, List<MenuCategoryModel> menuCategoryModelList) {
        this.context = context;
        this.menuCategoryModelList = menuCategoryModelList;
    }

    @NonNull
    @Override
    public MyCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyCategoryAdapter.MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_category_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyCategoryAdapter.MyViewHolder holder, int position) {

        Glide.with(context).load(menuCategoryModelList.get(position).getImage()).into(holder.imgCategory);
        holder.txtCategory.setText(new StringBuilder(menuCategoryModelList.get(position).getName()));

        holder.setListener(new IRecyclerClickListener() {
            @Override
            public void onItemClickListener(View view, int pos) {
                Cons.categorySelected = menuCategoryModelList.get(pos);
                EventBus.getDefault().postSticky(new CategoryClick(true, menuCategoryModelList.get(pos)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuCategoryModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        IRecyclerClickListener listener;

        public void setListener(IRecyclerClickListener listener) {
            this.listener = listener;

        }

        TextView txtCategory = itemView.findViewById(R.id.txtCategoryName);

        ImageView imgCategory = itemView.findViewById(R.id.imgCategory);

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            listener.onItemClickListener(v, getAdapterPosition());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (menuCategoryModelList.size() == 1) {
            return Cons.DEFAULT_COLUMN_COUNT;
        } else {
            if (menuCategoryModelList.size() % 2 == 0) {
                return Cons.DEFAULT_COLUMN_COUNT;
            } else {
                return (position > 1 && position == menuCategoryModelList.size() - 1 ? Cons.Full_Width_Column : Cons.DEFAULT_COLUMN_COUNT);
            }

        }

    }
}
